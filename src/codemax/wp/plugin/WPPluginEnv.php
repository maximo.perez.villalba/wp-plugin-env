<?php
namespace codemax\wp\plugin;

use codemax\tool\Path;
use codemax\tool\TXT;
use ErrorException;

class WPPluginEnv
{
    
    /**
     *
     * @var array
     */
    static private $pluginsEnv = [];
    
    /**
     *
     * @var string
     */
    static private $currentPluginsEnv = NULL;
    
    /**
     *
     */
    static public function init(): void
    {
        $backtrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT, 1 );
        
        $pluginEnv = new WPPluginEnv( $backtrace[ 0 ][ 'file' ] );
        
        self::$pluginsEnv[ $pluginEnv->urn() ] = $pluginEnv;
        self::current( $pluginEnv->urn() );
    }
    
    /**
     *
     * @param string $urn
     * @throws ErrorException
     * @return WPPluginEnv
     */
    static public function current( string $urn = NULL ): WPPluginEnv
    {
        if ( isset( $urn ) && isset( self::$pluginsEnv[ $urn ] ) )
        {
            self::$currentPluginsEnv = $urn;
        }
        if ( isset( self::$currentPluginsEnv ) && isset( self::$pluginsEnv[ self::$currentPluginsEnv ] ) )
        {
            return self::$pluginsEnv[ self::$currentPluginsEnv ];
        }
        throw new ErrorException( "Error: The current Env is not selected." );
    }
    
    /**
     *
     * @param string $urn
     * @return WPPluginEnv|NULL
     */
    static public function get( string $urn )
    {
        if ( isset( self::$pluginsEnv[ $urn ] ) )
        {
            return self::$pluginsEnv[ $urn ];
        }
        return NULL;
    }
    
    /**
     *
     * @return string
     */
    static public function dump(): string
    {
        $dump = static::class;
        $dump .= PHP_EOL;
        $dump .= print_r( self::$pluginsEnv, TRUE );
        $dump .= PHP_EOL;        
        return $dump;
    }
    
    /**
     *
     * @var string
     */
    private string $urn = '';
    
    /**
     * 
     * @var string
     */
    private string $dirbase = '';
    
    /**
     * 
     * @var string
     */
    private string $urlbase = '';
    
    /**
     * 
     * @var array
     */
    private array $prefix = [];

    /**
     * 
     * @param string $pluginAbsoluteFilePath
     */
    private function __construct( string $pluginAbsoluteFilePath )
    {
        $this->urn = basename( dirname( $pluginAbsoluteFilePath ) );
        $this->dirbase = Path::forceDirBase( plugin_dir_path( $pluginAbsoluteFilePath ) );
        $this->urlbase = plugin_dir_url( $pluginAbsoluteFilePath );
    }

    /**
     * 
     * @param string $key
     * @param string $prefix
     * @return string
     */
    public function prefix( string $key, string $prefix = NULL ): string
    {
        if ( isset( $prefix ) )
        {
            $prefix = Path::safe( $prefix );
            if ( TXT::create( $prefix )->startWith( DIRECTORY_SEPARATOR ) )
            { 
                $prefix = substr( $prefix, 1 );
            }
            $this->prefix[ $key ] = Path::forceDirBase( $prefix );
        }
        if ( isset( $this->prefix[ $key ] ) )
        {
            return $this->prefix[ $key ];
        }
        return '';
    }
    
    /**
     *
     * @return string
     */
    public function urn(): string
    {
        return $this->urn;
    }
    
    /**
     * 
     * @return string
     */
    public function dirbase(): string
    {
        return $this->dirbase;
    }
    
    /**
     *
     * @return string
     */
    public function urlbase(): string
    {
        return $this->urlbase;
    }
    
    /**
     *
     * @param string $relativePath
     * @param string $keyPrefix
     * @return string
     */
    public function path( string $relativePath = NULL, string $keyPrefix = NULL ): string
    {
        $path = $this->dirbase();
        if( isset( $keyPrefix ) && isset( $this->prefix[ $keyPrefix ] ) )
        {
            $path .= $this->prefix[ $keyPrefix ];
        }
        if( isset( $relativePath ) )
        {
            $relativePath = Path::safe( $relativePath );
            if( TXT::create( $relativePath )->startWith( DIRECTORY_SEPARATOR ) )
            { 
                $relativePath = substr( $relativePath , 1 );
            }
            $path .= $relativePath;
        }
        return $path;
    }

    /**
     * 
     * @param string $extension
     * @param string $keyPrefix
     * @return string
     */
    public function url( string $extension = NULL, string $keyPrefix = NULL ): string
    {
        $url = $this->urlbase();
        if( isset( $keyPrefix ) && isset( $this->prefix[ $keyPrefix ] ) )
        {
            $url .= str_replace( DIRECTORY_SEPARATOR,  '/', $this->prefix[ $keyPrefix ] );
        }
        if ( isset( $extension ) )
        {
            if( TXT::create( $extension )->startWith( '/' ) )
            { 
                $extension = substr( $extension , 1 );
            }
            $url .= $extension;
        }
        return $url;
    }
}